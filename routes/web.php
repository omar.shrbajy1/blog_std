<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();


//Auth::routes();
Route::group(['middleware' => 'user'], function () {

    Route::group([], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/post', 'user\PostController@getCreatePostPage');
        Route::post('/post', 'user\PostController@createPost');
        Route::get('/post/{id}', 'user\PostController@getPost');
        Route::get('/home', 'HomeController@index')->name('home');
        Route::post('/comment/{id}', 'user\CommentController@createComment');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::get('login', 'admin\AdminLoginController@getLoginPage');
        Route::post('login', 'admin\AdminLoginController@login')->name('admin.auth.loginAdmin');
        Route::get('/', 'admin\AdminController@getDashboard');
        Route::patch('/post/{id}', 'admin\PostController@switchPostStatus');
    });


});


Auth::routes();

