@extends('layouts.app')

@section('content')
    <div class="container home">

        @if(request()->guard == 'admin')
            @foreach($posts as $post)
                <div class="row">
                    <a href="#" class="url">
                        <div class="col-sm-12 post">
                            <div class="row">
                                <div class="img col-sm-4">
                                    <img src="/images/water.jpg" width="200px" height="200px"
                                         class="img-responsive" alt="">
                                </div>
                                <div class="  col-sm-8">

                                    <h1>title</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum eius error
                                        ipsum itaque modi molestiae nesciunt optio quibusdam repudiandae voluptate.
                                        Aperiam corporis ducimus eaque fuga, illo in incidunt odit temporibus.</p>
                                </div>
                            </div>

                            <a class="btn-info post-view">view post</a>
                        </div>
                    </a>


                </div>
            @endforeach
        @else

        @endif


        <br>

        <a href="post" class="btn btn-info btn-add" style="color:white">Add new post</a>

    </div>
@endsection
