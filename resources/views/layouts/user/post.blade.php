@extends('layouts.app')


@section('styles')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }

        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection
@section('content')



    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                @if(!$post)
                    <h1>Post does not exist</h1>
                    <?php return; ?>
                @endif
                <input type="hidden" name="postId" value="{{$post->id}}">
                @if(request()->guard == 'user')
                    <button class="btn btn-info">edit</button>
                    <button class="btn btn-info">delete</button>
                @elseif(request()->guard == 'admin')
                    <label class="switch">
                        <input name="toggle" type="checkbox" {{$post->allowed ? 'checked' : ''}}>
                        <span class="slider"></span>
                    </label>
                @endif

                <input type="hidden" id="postId" value="{{$post->id}}">

                <img src="{{'/images/' . $post->img_url}}" width="100%"
                     class="img-responsive" alt="">

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <br>
                <h1 style="font-weight: bolder">{{$post->title}}</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p style="font-size: 30px">
                    {{$post->body}}
                </p>

            </div>
        </div>

        <div class="row">


            <div class="col-sm-12">
                write comment
                <form id="commentForm">
                    <div class="form-group">
                        <textarea name="content" class="form-control">
            </textarea>
                    </div>
                    <button class="btn btn-info">submit</button>
                </form>
            </div>

        </div>

        @foreach($comments as $comment)

            <div class="row">
                <div class="col-sm-12" style="border:1px dashed gray;">
                    <h3>{{$comment->user->name}}</h3>
                    <p>{{$comment->title}}</p>
                </div>
            </div>



        @endforeach
    </div>






@endsection
