@extends('layouts.app')

@section('content')

    <form  enctype="multipart/form-data" method="post" action="{{action('user\PostController@createPost')}}">
        {{csrf_field()}}
        <div class="form-group">
            <label for="title">Title</label>
            <input name="title" type="text" class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="title">Title</label>
            <input name="img_url" type="file" class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="pwd">Body</label>
            <textarea name="body" class="form-control">
            </textarea>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
@endsection
