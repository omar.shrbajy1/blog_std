<?php

use App\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $admin = new Admin;
       $admin->name = 'omar';
       $admin->email = 'omar@gmail.com';
       $admin->password = bcrypt('123456');
    }
}
