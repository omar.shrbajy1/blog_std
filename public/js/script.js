$(function () {

    function getCommentTemplate(user, title) {
        return '<div class="row"><div class="col-sm-12" style="border: 1px dashed gray;"><h3>' + user + '</h3> <p>' + title + '</p></div></div>'
    }


    // $('#commentForm').on('submit', function (e) {
    //     e.preventDefault();
    //
    //
    //     $.ajax({
    //
    //         url: '/comment/' + $('#postId').val(),
    //         type: 'post',
    //         data: {
    //             content: $('textarea[name=content]').val()
    //         },
    //         headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
    //
    //
    //     })
    //
    //
    // })


    $('input[name=toggle]').on('change', function () {

        let y = $(this).is(':checked');

        $.ajax({
            url: '/admin/post/' + $('#postId').val(),
            data: {
                allowed: y ? 1 : 0
            },
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success:function (data) {
                console.log(data);
            },
            type:'patch'
        })


    })

    $('#commentForm').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            url: '/comment/' + $('input[name=postId]').val(),
            type: 'post',
            data: {
                title: $('textarea[name=content]').val()
            },
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                $('.container .container').append(getCommentTemplate(data.user, data.comment.title))
            }
        })


    })


});
