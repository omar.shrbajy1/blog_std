<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title' ,'body' , 'img_url' , 'allowed'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes(){
        return $this->belongsToMany(User::class , 'likes');
    }
}
