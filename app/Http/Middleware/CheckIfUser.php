<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckIfUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::guard('admin')->check()) {
            $request->guard = 'admin';
            return $next($request);
        }
        else if(Auth::guard('web')->check()){
            $request->guard = 'user';

            return $next($request);

        }
        return response(['message' => 'unauthorized'], 401);
    }
}
