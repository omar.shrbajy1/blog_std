<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */




    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $postsForAdmin = Post::all();
        $postsForUser = Post::where('allowed' ,1)->get();
        return view('home')->with(['userPosts' => $postsForUser , 'adminPosts' => $postsForAdmin]);
    }
}
