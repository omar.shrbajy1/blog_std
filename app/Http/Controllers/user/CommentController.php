<?php

namespace App\Http\Controllers\user;

use App\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Auth;
class CommentController extends Controller
{
    public function  createComment(Request $request, $postId){

        $validator = Validator::make($request->all() , [
            'title' => 'required|string'
        ]);


        if($validator->fails())
            return $validator->messages();
        $request['post_id'] = $postId;
        $request['user_id']= Auth::id();
        return ['comment' => Comment::create($request->all()) , 'user' => Auth::user()->name];



    }
}
