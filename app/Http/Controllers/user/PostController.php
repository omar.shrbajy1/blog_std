<?php

namespace App\Http\Controllers\user;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class PostController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

        public function getPost($id){

            $post = Post::find($id);
            $comments = $post->comments()->with('user')->get();
            return view('layouts.user.post')->with(['post' => $post, 'comments' => $comments]);

        }



    public function createPost(Request $request){

        $validator = Validator::make($request->all() , [
            'title' => 'required|string',
            'body' => 'required|string',
            'img_url' => 'nullable|file'
        ]);

        if($validator->fails())
            return $validator->messages();

        $post = new Post;
        $post->title = $request['title'];
        $post->body = $request['body'];
        if ($request->hasFile('img_url')) {
            $image = $request->file('img_url');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path('/images');
            $image->move($destinationPath, $name);
            $post->img_url = $name;
    }
        $post->save();
//        $post = Post::create($request->all());
        return redirect()->to('/home');

    }
    public function getCreatePostPage(){
        return view('layouts.user.create-post');
    }
}
