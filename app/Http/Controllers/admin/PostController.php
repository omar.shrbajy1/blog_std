<?php

namespace App\Http\Controllers\admin;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    function switchPostStatus(Request $request , $id){
        $post = Post::find($id);

        if(!$post)
            return response()->json(['message' => 'model not found'] , 400);


        $updated = $post->update(['allowed' => $request['allowed']]);
        return response()->json($updated);


    }
}
