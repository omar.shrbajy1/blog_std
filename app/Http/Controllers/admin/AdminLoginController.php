<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
   function login(Request $request){
//          return 1;
       if(Auth::guard('admin')->attempt(['email' => $request['email'] , 'password' => $request['password']])){

            return redirect()->to('admin');
       }

   }


   function getLoginPage(){
       return view('layouts.admin.login');

   }

}
