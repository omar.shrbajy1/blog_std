<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    function getDashboard()
    {
        return view('layouts.admin.dashboard');
    }
}
